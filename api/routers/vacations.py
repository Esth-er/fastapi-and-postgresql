# any time you want to add more fastapi endpoints

from fastapi import APIRouter
from queries.vacations import VacationIn


router = APIRouter()


@router.post("/vacations")
def create_vacation(vacation: VacationIn):
    print('vacation', vacation)
    print('from_date', vacation.from_date.month)
    return vacation
