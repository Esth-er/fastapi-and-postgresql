# whenever you want to write query sql to connect to your database

from pydantic import BaseModel
from typing import Optional
from datetime import date

# THIS HAS NOTHING TO DO WITH THE DATABASE
# BASEMODELS are for you to look at your endpoints
# this is the data thats coming into the program
class Thought(BaseModel):
    private_thoughts: str
    public_thoughts: str


class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[Thought]
# whenever the user submits json to us it will have all of this information
